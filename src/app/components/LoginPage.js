import React, { Component } from 'react';
import { loginUser, registerUser } from '../actions'
import { connect } from "react-redux";
import { Button } from 'reactstrap';
import './LoginPage.css';
class LoginPage extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      error: '',
      email: '',
      registerFormOpened: false,
    };

    this.handlePassChange = this.handlePassChange.bind(this);
    this.handleUserChange = this.handleUserChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.dismissError = this.dismissError.bind(this);
    this.handleRegisterClick = this.handleRegisterClick.bind(this);
  }

  dismissError() {
    this.setState({ error: '' });
  }

  handleSubmit(evt) {
    evt.preventDefault();
    if (!this.state.email) {
      return this.setState({ error: 'E-mail is required' });
    }

    if (!this.state.password) {
      return this.setState({ error: 'Password is required' });
    }

    if (evt.target.name === 'loginForm') {
      this.props.loginUser();
    } else {
      this.props.registerUser();
    }
    
    return this.setState({ error: '' });
  }

  handleUserChange(evt) {
    this.setState({
      username: evt.target.value,
    });
  };

  handlePassChange(evt) {
    this.setState({
      password: evt.target.value,
    });
  }

  handleEmailChange(evt) {
    this.setState({
      email: evt.target.value,
    });
  }

  handleRegisterClick(evt) {
    this.setState({
      registerFormOpened: !this.state.registerFormOpened
    })
  }


  render() {
    // NOTE: I use data-attributes for easier E2E testing
    // but you don't need to target those (any css-selector will work)
    if (this.state.registerFormOpened) {
      return (
        <div className="Register">
          <form onSubmit={this.handleSubmit} name="registerForm">
            <div className="form-group white">
              {
                  this.state.error &&
                  <span data-test="error" onClick={this.dismissError}>                
                      {this.state.error}
                  </span>
              }
            </div>
            <div className="form-group">
              <input type="text" data-test="email" placeholder="E-mail" value={this.state.email} onChange={this.handleEmailChange} />
            </div>

            <div className="form-group">
              <input type="text" data-test="username" placeholder="Username" value={this.state.username} onChange={this.handleUserChange} />
            </div>

            <div className="form-group">
              <input type="password" data-test="password" placeholder="Password" value={this.state.password} onChange={this.handlePassChange} />
            </div>
            
            <input type="submit" value="Register" data-test="submit" />

          </form>
          
          <div>
            <p>Already a member?</p>
            <Button onClick={this.handleRegisterClick} color="success">Lets login</Button>
          </div>
        </div>
      );
    }

    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit} name="loginForm">
          <div className="form-group white">
            {
                this.state.error &&
                <span data-test="error" onClick={this.dismissError}>                
                    {this.state.error}
                </span>
            }
          </div>
          <div className="form-group">
            <input type="text" data-test="email" placeholder="E-mail" value={this.state.email} onChange={this.handleEmailChange} />
          </div>

          <div className="form-group">
            <input type="password" data-test="password" placeholder="Password" value={this.state.password} onChange={this.handlePassChange} />
          </div>
          
          <input type="submit" value="Log In" data-test="submit" />

        </form>
        
        <div>
          <p>Not a member?</p>
          <Button onClick={this.handleRegisterClick} color="success">Create an account</Button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
    return Object.assign({}, state);
}

export default connect( mapStateToProps, { loginUser, registerUser } )(LoginPage);
