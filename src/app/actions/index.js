import { USER_LOGIN, USER_REGISTER } from "./action-types";

export function loginUser() {
  return { type: USER_LOGIN}
}

export function registerUser() {
  return { type: USER_REGISTER }
}