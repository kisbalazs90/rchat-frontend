import { takeEvery, call, put } from "redux-saga/effects";
import { USER_LOGIN, DID_USER_LOGIN , USER_REGISTER, DID_USER_REGISTER} from "../actions/action-types"
import * as axios from "axios";

export default function* watcherSaga() {
  yield takeEvery(USER_LOGIN, workerSaga);
  yield takeEvery(USER_REGISTER, workerSaga);
}

function* workerSaga() {
  try {
    const loginData = yield call(postLogin);
    yield put({ type: DID_USER_LOGIN, loginData });

    const registerData = yield call(postRegister);
    yield put({ type: DID_USER_REGISTER, registerData});

  } catch (e) {
    yield put({ type: "API_ERRORED", payload: e });
  }
}

function  postLogin() {
  console.log('postLogin called');
  return  axios.post("http://127.0.0.1:3000/auth/login", {email:"kisbalazs90@gmail.com", password:"123456"}).then(response =>
    response.data.token
  );
}

function  postRegister() {
    console.log('postRegister called');
    return  axios.post("http://127.0.0.1:3000/users", {name: "Kis Balázs", email:"kisbalazs90@gmail.com", password:"123456"}).then(response =>
      response.data.token
    );
}
