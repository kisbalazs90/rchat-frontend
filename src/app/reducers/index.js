import { DID_USER_LOGIN } from "../actions/action-types";


function rootReducer(state , action) {
  if (action.type === DID_USER_LOGIN) {
    return Object.assign({}, state, {
      token: action.payload
    });
  }
  
  return state;
}

export default rootReducer;
